# Lab 2 -- Linter and SonarQube as a part of quality gates


## Homework

As a homework you will need to add automatical SonarCloud check to your CI. SonarCloud is a simplified version of SonarQube. [you can find it here](https://sonarcloud.io). Once again, lab should consist all of the checks, for this lab you may mark your SonarCloud check with `allow_failure: true`, Your resulting pipeline should have three steps: `build` -> `linter` -> `sonar_cloud`. As well you should attach screenshots of SonarQube check to your readme file.
**Lab is counted as done, if your pipelines are passing.**

![](sonarcube.png)
